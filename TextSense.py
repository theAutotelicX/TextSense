from textblob.wordnet import Synset
from textblob import Word
from delStopWord import delStopWord
from nltk.corpus.reader.wordnet import WordNetError
from nltk.wsd import lesk
from nltk.tokenize import word_tokenize
import re
#initialize category
class TextSense:
	#assign global variable here
	totalSimInCate=[0.0,0.0,0.0,0.0,0.0,0.0,0.0]
	def __init__(self):
		#food group
		self.syn = []
		self.syn.append(Synset("food.n.01"))
		self.syn.append(Synset("beverage.n.01"))
		self.syn.append(Synset("nutrition.n.01"))
		self.syn.append(Synset("drink.n.01"))
		self.syn.append(Synset("juice.n.01"))
		self.syn.append(Synset("vegetable.n.01"))
		self.syn.append(Synset("meat.n.01"))
		#technology group
		self.syn.append(Synset("technology.n.01"))
		self.syn.append(Synset("telephone.n.01"))
		self.syn.append(Synset("computer.n.01"))
		self.syn.append(Synset("device.n.01"))
		self.syn.append(Synset("modernity.n.01"))
		self.syn.append(Synset("artificial_intelligence.n.01"))
		self.syn.append(Synset("future.n.01"))
		#travel group
		self.syn.append(Synset("travel.n.01"))
		self.syn.append(Synset("sea.n.01"))
		self.syn.append(Synset("mountain.n.01"))
		self.syn.append(Synset("conveyance.n.03"))
		self.syn.append(Synset("river.n.01"))
		self.syn.append(Synset("tourism.n.01"))
		self.syn.append(Synset("forest.n.01"))
		#education group
		self.syn.append(Synset("subject.n.03"))
		self.syn.append(Synset("study.n.09"))
		self.syn.append(Synset("education.n.02"))
		self.syn.append(Synset("learning.n.01"))
		self.syn.append(Synset("school.n.01"))
		self.syn.append(Synset("skill.n.01"))
		self.syn.append(Synset("research.n.01"))
		#sport group
		self.syn.append(Synset("sport.n.01"))
		self.syn.append(Synset("league.n.01"))
		self.syn.append(Synset("football.n.01"))
		self.syn.append(Synset("contest.n.01"))
		self.syn.append(Synset("exercise.n.01"))
		self.syn.append(Synset("tournament.n.01"))
		self.syn.append(Synset("game.n.01"))
		#politic group
		self.syn.append(Synset("politics.n.01"))
		self.syn.append(Synset("government.n.01"))
		self.syn.append(Synset("minister.n.02"))
		self.syn.append(Synset("war.n.02"))
		self.syn.append(Synset("economy.n.01"))
		self.syn.append(Synset("law.n.01"))
		self.syn.append(Synset("election.n.01"))
		#health group
		self.syn.append(Synset("health.n.01"))
		self.syn.append(Synset("mind.n.01"))
		self.syn.append(Synset("disease.n.01"))
		self.syn.append(Synset("medicine.n.02"))
		self.syn.append(Synset("cancer.n.01"))
		self.syn.append(Synset("diet.n.01"))
		self.syn.append(Synset("illness.n.01"))
	def initSynsetWord():
		setWord = []
		for word in filter_text:
			try:
				trial = lesk(word_tokenize(readSentence),word,'n')
				# print(word,' ',trial)
				if trial is not None:
					setWord.append(trial)
			except (WordNetError,AttributeError):
				pass
		return setWord

	def findSim(self,i):
		# print(i,'\n')
		counter=0;
		total=0.0;
		inCategory = []
		for synWord in self.syn:
			if counter < 7:
				pass
			else:
				counter=0;
				inCategory.append(total)
				total =0.0;
			counter+=1
			total += synWord.path_similarity(i)
		inCategory.append(total)
		# print(inCategory)
		return inCategory
	def eval(self,inCategory):
		for i in range(len(inCategory)):
			TextSense.totalSimInCate[i] += inCategory[i]
	def calPercentage(self):
		totalValueCate = sum(TextSense.totalSimInCate)
		print("Calculating the probability of this sentence to be in each category ")
		print("-->",readSentence)
		print("--------------------------------------------------------------")
		print("Food Category: ",round(TextSense.totalSimInCate[0] * 100 / totalValueCate,2),"%")
		print("Technology Category: ",round(TextSense.totalSimInCate[1] * 100 / totalValueCate,2),"%")
		print("Tourism Category: ",round(TextSense.totalSimInCate[2] * 100 / totalValueCate,2),"%")
		print("Education Category: ",round(TextSense.totalSimInCate[3] * 100 / totalValueCate,2),"%")
		print("Sport Category: ",round(TextSense.totalSimInCate[4] * 100 / totalValueCate,2),"%")
		print("Politic Category: ",round(TextSense.totalSimInCate[5] * 100 / totalValueCate,2),"%")
		print("Health Category: ",round(TextSense.totalSimInCate[6] * 100 / totalValueCate,2),"%")
		print("--------------------------------------------------------------\n")

		indexMax = TextSense.totalSimInCate.index(max(TextSense.totalSimInCate))
		if indexMax == 0:
			ans = "Food Category"
		elif indexMax ==1:
			ans = "Technology Category"
		elif indexMax ==2:
			ans = "Tourism Category"
		elif indexMax ==3:
			ans = "Education Category"
		elif indexMax ==4:
			ans = "Sport Category"
		elif indexMax ==5:
			ans = "Politic Category"
		elif indexMax ==6:
			ans = "Health Category"
		else:
			ans = "Not Found"
		print("Guess category: ",ans)
#initalize all categories
#
# use readTextLine to grab the sentence from sampleTopic.txt
# @param readTextLine refer select the number of line in the text file
# e.g. = readTextLine = 1
# will read from the first '\n' to second '\n'
#
readTextLine = 2;
##################################################################
sensing = TextSense()
readSentence = delStopWord.readFile(readTextLine)
# print(readSentence)
filter_text = delStopWord.sentenceCorrection()
synsetWord = TextSense.initSynsetWord()
for i in synsetWord:
	inCategory = sensing.findSim(i)
	sensing.eval(inCategory)
for i in range((len(inCategory))):
	sensing.totalSimInCate[i] /= len(filter_text)
sensing.calPercentage()



# ask = Word("meat")
# print(ask.definitions[:5])
# ask = Synset('biscuit.n.01')
# print(ask.synsets[:5])
