from nltk.tokenize import word_tokenize
from textblob import TextBlob,Word
from nltk.corpus import stopwords
class delStopWord:
	sentence =[]
	def readFile(index):
		read = open('sampleTopic.txt','r')
		i=0
		while True:
			line = read.readline()
			if index == i:
				global sentence
				sentence = line
				return sentence
			if not line:
				break
			i+=1
		read.close()

	def sentenceCorrection():
		word_tokens = word_tokenize(sentence)
		stop_words = set(stopwords.words('english'))
		#filtered_sentence = [w for w in word_tokens if not w in stop_words]
		extend_stop_words = ["i",",",".","still","#"]
		filtered_sentence = []
		for w in word_tokens:
			w = w.lower()
			if w not in stop_words and w not in extend_stop_words:
				word = Word(w)
				word = word.singularize()
				filtered_sentence.append(word)

		return filtered_sentence
